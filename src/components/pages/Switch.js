import React, { useState, useEffect, useContext } from "react"
import { Link, withRouter } from "react-router-dom"
// import queryString from 'query-string';
import Web3 from 'web3'

import Breakpoint from 'react-socks';
import { Row, Form, Col, Modal, Input, message, Table, Spin } from 'antd'

import { Context } from '../../context'
import Binance, { isAddressBNB, notExchangeBNB } from "../../clients/binance"
import { isAddressETH, getETHBalance, getRUNEBalance } from "../../clients/web3"
import { handleTx } from "../../clients/wallet"
import { AmounttoString, shortenHash, getAddrETHLink, getTxETHLink, getAddrBNBLink, getTxBNBLink } from '../../utils/utility'
import { SYMBOL, BASE_URL, DEX_URL, ETHERSCAN_URL } from '../../env'
import { H1, Button, Text, Coin, WalletAddress } from "../Components"

import axios from "axios";

const PATH_ADDRESS = 'address'
const RETRIES = 8
const SWITCHFEE = '5'
const MIN_SWITCH = 100
const MAX_SWITCH = 25000

const Switch = (props) => {
  const [selectedCoin, setSelectedCoin] = useState(null)
  const [balances, setBalances] = useState(null)
  const [mode, setMode] = useState("stake")
  const [loadingBalances, setLoadingBalances] = useState(false)
  const [price, setPrice] = useState(null)
  const [fee, setFee] = useState(null)

  const [bridgeTxData, setBridgeTxData] = useState(null)
  const [bridges, setBridges] = useState({ "BNB": "bnb123..123", "ETH": "0x123...123" })

  const [count, setCount] = useState(RETRIES + 1)
  const [chain, setChain] = useState(null)

  // confirmation modal variables
  const [visible, setVisible] = useState(false)
  const [sending, setSending] = useState(false)

  const context = useContext(Context)

  useEffect(() => {
    // let params = queryString.parse(props.location.search)
    //console.log(context.chain)
    setChain(context.chain)

    getPrice()
    getFee()
    getBridges()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  useEffect(() => {
    getBalances()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [context.wallet])

  const delay = ms => new Promise(res => setTimeout(res, ms));

  useEffect(() => {
    refreshTable()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [count])

  useEffect(() => {
    if (count === RETRIES) {
      getBalances()
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [count])

  const refreshTable = async () => {
    if (count < RETRIES) {
      await delay(10000)
      getBridgeData(context.wallet?.address)
      setCount(count + 1)
    }

  }

  const getPrice = () => {
    Binance.price(SYMBOL)
      .then((response) => {
        //console.log("Result", response)
        setPrice(response)
      })
      .catch((error) => {
        console.error(error)
      })
  }

  const getFee = () => {
    setFee(0.00037500)
  }

  const getBridges = async () => {
    try {
      let resp = await axios.get(`${BASE_URL}`)
      //console.log(resp.data.bridges)
      setBridges(resp.data.bridges)
    }
    catch (error) {
      console.error(error)
    }
  }

  const getBalances = async () => {
    if (context.wallet && context.wallet.address) {
      setLoadingBalances(true)

      if (!context.web3Wallet) {
        try {
          let response = await Binance.getBalances(context.wallet.address)
          //console.log("Balances:", response)

          const b = (response || []).map((bal) => (
            {
              "icon": bal.symbol === SYMBOL ? "coin-rune" : "coin-bep",
              "ticker": bal.symbol,
              "free": parseFloat(bal.free),
              "frozen": parseFloat(bal.frozen),
              "locked": parseFloat(bal.locked),
            }
          ))
          let relevantBalances = b.filter((x) => x.ticker === SYMBOL || x.ticker === 'BNB')

          //console.log(relevantBalances)
          setBalances([...relevantBalances])
          setLoadingBalances(false)
        } catch (error) {
          setLoadingBalances(false)
        }

      } else {

        let rune_bal = await getRUNEBalance(context.wallet.address)
        let eth_bal = await getETHBalance(context.wallet.address)

        let runeBalance = {
          "icon": "coin-rune",
          "ticker": SYMBOL,
          "free": Number(Web3.utils.fromWei(rune_bal)),
          "frozen": 0,
          "locked": 0
        }

        let ethBalance = {
          "icon": "coin-bep",
          "ticker": 'ETH',
          "free": Number(Web3.utils.fromWei(eth_bal)),
          "frozen": 0,
          "locked": 0
        }

        let relevantBalances = [runeBalance, ethBalance]
        //console.log(relevantBalances)
        setBalances([...relevantBalances])

        setLoadingBalances(false)

      }

      getBridgeData(context.wallet.address)
    }
  }

  const getBridgeData = async (address) => {
    if (address) {
      // //console.log(`${BASE_URL}/${PATH_ADDRESS}/${address}/${context.chain}`)
      let bridgeTx = await axios.get(`${BASE_URL}/${PATH_ADDRESS}/${address}/${context.chain}`)
      // //console.log(`Bridge Data: ${bridgeTx.data}`)
      setBridgeTxData(bridgeTx.data.jobs.reverse())
    }

  }

  const confirmation = (mode) => {
    if (!context.web3Wallet) {

      const bnb_bal = balances.find((b) => {
        return b.ticker === "BNB"
      }) || { free: 0 }
      if (fee > bnb_bal.free) {
        message.error("Insufficient fund: Not enough for transaction fee of " + fee + "BNB", 10)
        return
      }
      setMode(mode)
      setVisible(true)

    } else {

      const eth_bal = balances.find((b) => {
        return b.ticker === "ETH"
      }) || { free: 0 }
      if (0.01 > eth_bal.free) {
        message.error("Insufficient fund: Not enough for transaction fee of 0.01 ETH", 10)
        return
      }
      setMode(mode)
      setVisible(true)

    }

  }

  const handleOk = async (values) => {
    // Send coins!
    if (!context.wallet || !context.wallet.address) {
      //console.log("No wallet detected!")
      return
    }

    setSending(true)

    try {
      let bridgeAddress = chain === "BNB" ? bridges.BNB : bridges.ETH
      // let contract = chain === "ETH" ? null : 
      await handleTx(bridgeAddress, values, context)
      //console.log("Response", response)
      message.success(<Text color={'#000'}>Confirm in your wallet and wait ~30 seconds.</Text>, 10)
      setCount(0)
      setSending(false)
      setVisible(false)
      getBalances()
      getBridgeData(context.wallet.address)
    } catch (error) {
      message.error(error.message)
      setSending(false)
      setVisible(false)
      console.error(error)
    }

  }


  const handleCancel = () => {
    setVisible(false)
  }

  const passwordRequired = context.wallet && 'keystore' in context.wallet

  var balance = 0
  var dollarValue = "loading"
  if (balances) {
    try {
      balance = ((balances || []).find((b) => {
        return b.ticker === SYMBOL
      }).free +
        balances.find((b) => {
          return b.ticker === SYMBOL
        }).frozen
      )
    } catch (err) {
      console.error("No token balances on address")
      return
    }
    if (price) {
      dollarValue = "$" + (Number(balance) * Number(price)).toFixed(2)
    }
  }

  const thisChain = () => {
    return context.chain === "BNB" ? "Binance Chain" : "Ethereum"
  }

  const otherChain = () => {
    return context.chain === "BNB" ? "Ethereum" : "Binance Chain"
  }

  const refresh = () => {
    setCount(5)
  }

  // styling
  const coinRowStyle = {
    margin: "10px 0px",
    marginLeft: "10px",
    marginRight: "10px",
    marginTop: "20px",
  }

  const paneStyle = {
    backgroundColor: "#48515D",
    marginRight: "20px",
    marginTop: "20px",
    borderRadius: 5,
  }

  return (

    <div style={{ marginTop: 20, marginLeft: 5 }}>

      <Row>

        <Col xs={24} sm={24} md={1} lg={2}>
        </Col>

        <Col xs={24} sm={24} md={22} lg={20}>
          <div>
            <H1>SWITCH RUNE</H1>
          </div>

          <div>
            <Text size={18}>
              Switch RUNE from {thisChain()} to {otherChain()}.
          </Text>

          </div>

          <div style={{ marginTop: "20px" }}>

            <Breakpoint small down>
              {!loadingBalances && context.wallet &&
                <Row>
                  <Col xs={24} sm={24} md={12} style={{ marginTop: "20px" }}>
                    {context.chain === "BNB" &&
                      < a target="_blank" rel="noopener noreferrer" href={`${DEX_URL}/address/` + context.wallet.address}>
                        <WalletAddress />
                      </a>
                    }
                    {context.chain === "ETH" &&
                      < a target="_blank" rel="noopener noreferrer" href={`${ETHERSCAN_URL}/address/` + context.wallet.address}>
                        <WalletAddress />
                      </a>
                    }
                  </Col>
                </Row>
              }
            </Breakpoint>

            <Breakpoint medium up>
              {!loadingBalances && context.wallet &&
                <Row>
                  <Col xs={24} sm={24} md={12} style={{ marginTop: "20px" }}>
                    {context.chain === "BNB" &&
                      < a target="_blank" rel="noopener noreferrer" href={`${DEX_URL}/address/` + context.wallet.address}>
                        <WalletAddress />
                      </a>
                    }
                    {context.chain === "ETH" &&
                      < a target="_blank" rel="noopener noreferrer" href={`${ETHERSCAN_URL}/address/` + context.wallet.address}>
                        <WalletAddress />
                      </a>
                    }

                  </Col>
                </Row>
              }
            </Breakpoint>

            <Row style={{ marginTop: "40px" }}>
              {loadingBalances && context.wallet &&
                <Text><i>Loading balances, please wait...</i></Text>
              }

              {!context.wallet &&
                <Link to={`/wallet/unlock`}><Button fill>CONNECT WALLET</Button></Link>
              }

              {!loadingBalances && context.wallet && (balances || []).length === 0 &&
                <Text>No coins available</Text>
              }
            </Row>

            <Row>
              <Col xs={24}>

                {!loadingBalances && context.wallet && (balances || []).length > 0 &&
                  <Row style={{ marginBottom: "50px" }}>

                    <Col xs={24} sm={6} style={paneStyle}>

                      <Row style={{ marginTop: "10px", marginLeft: "10px" }}>
                        <Col xs={24} style={{ paddingRight: "10px" }}>
                          <span><Text size={18}>SELECT RUNE BELOW</Text></span>
                          <div style={{ float: "right" }}>
                            <Button onclick={getBalances} transparent={true}><span>REFRESH</span></Button>
                          </div>
                          <hr />
                          
                        </Col>
                      </Row>


                      {!loadingBalances && (balances || []).map((coin) => (
                        <Row key={coin.ticker} style={coinRowStyle}>
                          <Col xs={24}>
                            <Coin {...coin} onClick={setSelectedCoin} border={selectedCoin === coin.ticker} />
                          </Col>
                        </Row>
                      ))
                      }
                    </Col>

                    <Col xs={24} sm={17} style={{
                      backgroundColor: "#48515D",
                      marginTop: "20px",
                      borderRadius: 5,
                    }}>

                      {selectedCoin && selectedCoin === SYMBOL &&
                        <Row style={{ marginTop: "10px", marginLeft: "10px" }}>
                          <Col xs={24}>

                            <Row>
                              <Col xs={24}>
                                <Text size={18}>SWITCH RUNE</Text>
                                <hr />
                                <Row>
                                  <Col xs={12} style={{ marginTop: "10px" }}>
                                    <span>
                                      <Text>TOTAL BALANCE:</Text>
                                    </span>
                                    <span style={{ margin: "0px 20px" }} size={22}>
                                      {AmounttoString(balance)} ({dollarValue})
                              </span>

                                  </Col>
                                </Row>
                              </Col>


                            </Row>
                            <br></br>
                            <Row>
                              <Col xs={24}>
                                <Row>
                                  <Col xs={24} sm={12}>
                                    <Row>
                                      <Col>
                                        <span>
                                          <Text>FREE:</Text>
                                        </span>
                                        <span style={{ margin: "0px 20px" }} size={22}>{balances.find((b) => {
                                          return b.ticker === SYMBOL
                                        }).free}
                                        </span>
                                      </Col>
                                    </Row>
                                    <Row style={{ marginBottom: 30 }}>
                                      <Col>
                                        <Button
                                          style={{ height: 40, width: 200, marginTop: 10 }}
                                          onClick={() => { confirmation('SWITCH RUNE') }}
                                          loading={sending}
                                        >
                                          SWITCH
                                </Button>
                                      </Col>
                                    </Row>
                                  </Col>

                                  <Col xs={24} sm={12}>
                                    <Row>
                                      <Col>
                                        <span>
                                          <Text>FROZEN:</Text>
                                        </span>
                                        <span style={{ margin: "0px 20px" }} size={22}>{balances.find((b) => {
                                          return b.ticker === SYMBOL
                                        }).frozen}
                                        </span>
                                      </Col>
                                    </Row>

                                    <Row>
                                      <Col>
                                        {/* <Button secondary
                                style={{height:40, width:200, marginTop: 10}}
                                onClick={() => { confirmation('WITHDRAW') }}
                                loading={sending}
                              >
                                UNFREEZE
                              </Button>
                              <br></br> */}
                                      </Col>
                                    </Row>
                                  </Col>
                                </Row>
                              </Col>
                            </Row>
                          </Col>
                        </Row>
                      }

                    </Col>
                    <Col xs={24} style={{ marginTop: 50 }}>
                      <Text size={18}>TRANSACTION HISTORY</Text>
                      <div style={{ float: "right" }}>
                        {count < RETRIES &&
                          <Spin size="large"></Spin>
                        }
                        {count === RETRIES &&
                          <Button onclick={refresh} text={true}><span>REFRESH</span></Button>
                        }
                      </div>
                      <br />
                      <hr />
                      <br />
                      <TxTable dataSource={bridgeTxData} chain={context.chain} />
                    </Col>
                  </Row>
                }
              </Col>
            </Row>
            <br />
            <Text size={16}>
              Terms:
              </Text>
            <br />
            <Text size={14}>
              <ul>
                <li>
                  Minimum of {MIN_SWITCH} RUNE. Maximum of {MAX_SWITCH} RUNE.
                </li>
                <li>
                  Bridge fee of {SWITCHFEE} RUNE. (to prevent abuse and pay for outbound gas costs).
                </li>
                <li>
                  Test bridge with a small amount first. This service will not automatically refund if an error is made.
                </li>
                <li>
                  Any issues, reach out to the team on telegram with transaction hashes.
                </li>
              </ul>
            </Text>
          </div>

          <Modal
            title={mode.charAt(0).toUpperCase() + mode.slice(1)}
            visible={visible}
            footer={null}
            onCancel={handleCancel}
            bodyStyle={{ backgroundColor: "#101921", paddingBottom: 0 }}
            headStyle={{ backgroundColor: "#2B3947", color: "#fff" }}
          >
            <WrappedSwitchForm chain={context.chain} fee={fee} password={passwordRequired} button={mode} onSubmit={handleOk} onCancel={handleCancel} loading={sending} />
          </Modal>

        </Col>

        <Col xs={24} sm={24} md={1} lg={2}>
        </Col>
      </Row>
    </div >
  )
}

const SwitchForm = (props) => {
  const handleSubmit = e => {
    e.preventDefault();
    props.form.validateFields((err, values) => {
      if (!err) {
        if (props.onSubmit) {
          props.onSubmit(values)
        }
        props.form.resetFields()
      }
    });
  };

  const { getFieldDecorator } = props.form;

  return (
    <Form onSubmit={handleSubmit} className="login-form">
      <Form.Item >
        {getFieldDecorator('amount', {
          rules: [{ required: true, message: 'Please input an amount of RUNE to switch.' },
          {
            validator(rule, amount, callback) {
              if (!(+amount >= MIN_SWITCH)) {
                callback(`Minimum of ${MIN_SWITCH} RUNE`);
              }
              if (!(+amount <= MAX_SWITCH)) {
                callback(`Maximum of ${MAX_SWITCH} RUNE`);
              }
              callback();
            }
          }],
        })(
          <Input
            placeholder={`Amount: ie 1200 (min of ${MIN_SWITCH} RUNE, maximum of ${MAX_SWITCH})`}
          />,
        )}
      </Form.Item>

      <Form.Item >
        {getFieldDecorator('address', {
          rules: [{ required: true, message: 'Please input the destination address you wish to receive your RUNE at:' },
          {
            validator(rule, address, callback) {
              if (!isAddressETH(address) && props.chain === "BNB") {
                callback("Not a valid Ethereum address");
              }
              if (!isAddressBNB(address) && props.chain === "ETH") {
                callback("Not a valid Binance Chain address");
              }
              if (!notExchangeBNB(address) && props.chain === "ETH") {
                callback("Do not use exchange addresses");
              }

              callback();
            }
          }],
        })(
          <Input
            placeholder="Enter your destination address to receive RUNE at:"
          />,
        )}
      </Form.Item>
      {props.password &&
        <Form.Item >
          {getFieldDecorator('password', {
            rules: [{ required: true, message: 'Please input your Password!' }],
          })(
            <Input
              type="password"
              placeholder="Password"
            />,
          )}
        </Form.Item>
      }
      <Form.Item>
        <div style={{ float: "right" }}>
          <Button onClick={props.onCancel} >Cancel</Button>
          <Button
            type="primary"
            htmlType="submit"
            onClick={handleSubmit}
            loading={props.loading}
            style={{ marginLeft: 10 }}
          >
            {props.button.charAt(0).toUpperCase() + props.button.slice(1)}
          </Button>
          <div style={{ padding: 0, margin: 0 }}>
            {props.chain === "BNB" &&
              <Text style={{ float: "right" }} size={12} color="#EE5366">Network Fee: {props.fee} BNB; Switch Fee: {SWITCHFEE} RUNE</Text>
            }
            {props.chain === "ETH" &&
              <Text style={{ float: "right" }} size={12} color="#EE5366">Network Fee: ~0.001 ETH; Switch Fee: {SWITCHFEE} RUNE</Text>
            }
          </div>
        </div>
      </Form.Item>
    </Form>
  );
}

const WrappedSwitchForm = Form.create({ name: 'staking' })(SwitchForm);

export default withRouter(Switch)


const TxTable = (props) => {


  const columns = [
    {
      title: 'TRANSACTION',
      dataIndex: 'id',
      render: record => <a href={getTxBNBLink(record)} target="blank">{shortenHash(record)}</a>,
    },
    {
      title: 'DATE',
      render: record =>
        <>
          {props.chain === "BNB" &&
            <>{record.in.time}</>
          }
          {props.chain === "ETH" &&
            <>{record.out.time}</>
          }
        </>,
    },
    {
      title: 'STATUS',
      dataIndex: 'status',
      key: 'status',
    },
    {
      title: 'AMOUNT',
      dataIndex: 'in',
      render: record => <>{record.amount}</>,
    },
    {
      title: 'RECIPIENT',
      dataIndex: 'out',
      render: record =>
        <>
          {props.chain === "BNB" &&
            <a href={getAddrETHLink(record.to)} target="blank">{shortenHash(record.to)}</a>
          }
          {props.chain === "ETH" &&
            <a href={getAddrBNBLink(record.to)} target="blank">{shortenHash(record.to)}</a>
          }
        </>,
    },
    {
      title: 'TX',
      // dataIndex: 'out',
      render: record =>
        <>
          {props.chain === "BNB" &&
            <a href={getTxETHLink(record.out.hash)} target="blank">{shortenHash(record.out.hash)}</a>
          }
          {props.chain === "ETH" &&
            <a href={getTxBNBLink(record.out.hash)} target="blank">{shortenHash(record.out.hash)}</a>
          }
        </>,
    },
  ];

  return (
    <Col xs={24}
      style={{ backgroundColor: '#D8D8D8', borderRadius: 5, paddingBottom: 5 }}>
      <Table dataSource={props.dataSource} columns={columns} pagination={false} size={'middle'} />
    </Col>

  )
}

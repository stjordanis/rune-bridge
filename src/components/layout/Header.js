import React, { useContext, useEffect } from 'react'
import { Link } from "react-router-dom"
import { Layout, Row, Col } from 'antd';

import Breakpoint from 'react-socks';

import { Context } from '../../context'
import { NET } from '../../env'

import { Icon, WalletAddrShort, PillText } from '../Components'

const Header = (props) => {

  const context = useContext(Context)

  useEffect(() => {
  }, [])

  const styles = {
    position: 'fixed',
    zIndex: 1,
    width: '100%',
    backgroundColor: "#2B3947",
  }

  return (

    <Layout.Header className="header-container" style={styles} >
      <Row>
        <Breakpoint medium up>

          <Col sm={12}>
            <div style={{ float: "left", display: "flex" }}>
              <span style={{ marginLeft: 20, marginRight: 20 }}>
                <Link to="/">
                  <Icon icon="runelogo" style={{ height: 40 }} />
                </Link>
              </span>
              {context.wallet &&
                <span style={{ marginRight: 20 }}>
                  <Link to="/switch">
                    <h3 style={{ color: '#4FE1C4' }}><strong>SWITCH</strong></h3>
                  </Link>
                </span>
              }
              <span>
                <Link to="/pools">
                  <h3 style={{ color: '#4FE1C4' }}><strong><span role="img" aria-label="sushi">🍣</span> POOLS</strong></h3>
                </Link>
              </span>
            </div>
          </Col>

          <Col sm={12}>
            <div style={{ float: "right" }}>
              <span style={{ marginRight: 10 }}><PillText color={'#616053'}>{NET}</PillText></span>
              {context.wallet &&
                <Link to="/wallet"><WalletAddrShort /></Link>
              }
            </div>
          </Col>
        </Breakpoint>

        <Breakpoint small down>
          <Col xs={24}>
            <div style={{ float: "left", display: "flex" }}>
              <span style={{ marginLeft: 20, marginRight: 20 }}>
                <Link to="/">
                  <Icon icon="runelogo" style={{ height: 40 }} />
                </Link>
              </span>
              <span>
                <Link to="/pools">
                  <h3 style={{ color: '#4FE1C4' }}><strong>POOLS</strong></h3>
                </Link>
              </span>
            </div>
          </Col>
        </Breakpoint>


      </Row>
    </Layout.Header>



  )
}

export default Header
